const dotenv = require("dotenv").config();

const DBURL = process.env.DBURL;
const EMAIL = process.env.EMAIL;
const PASSWORD = process.env.PASSWORD;
const PORT = process.env.PORT || 8000;

module.exports = {
  DBURL,
  EMAIL,
  PASSWORD,
  PORT,
};
