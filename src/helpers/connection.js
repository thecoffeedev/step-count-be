const { DBURL, PORT } = require("./environment");
const mongoose = require("mongoose");

const connectDb = new Promise((resolve, reject) => {
  try {
    mongoose.connect(DBURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log(`::: Database connection successful on url ${DBURL}`);
    resolve(PORT);
  } catch (error) {
    console.error(error.message);
    reject(error);
  }
});

module.exports = connectDb;
