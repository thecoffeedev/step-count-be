const express = require("express");
// const nodemailer = require("nodemailer");
const cors = require("cors");
const connectDb = require("./src/helpers/connection");
const steps = require("./src/routes/steps");

const app = express();
app.use(express.json());
app.use(cors());

app.use("/steps", steps);

connectDb
  .then((port) => {
    app.listen(port, () =>
      console.log(`::: Server is listening on PORT ${port} :::`)
    );
  })
  .catch((error) => {
    console.error(error);
  });
